# Notes

Noel Bautista's (@nbau21) very opinionated workflow for personal notes. Only works with the best text editor, vim.

This README is intentionally not encrypted.

## How to use

### Open and Edit file

`vime calendar.md`

### Save changes

`:NoteSave`

### Initial Setup

Create a `.vimencryptrc` file. Put the following inside:

```
source ~/.vimrc
set nobackup
set noswapfile
set nowritebackup
set cm=blowfish2
set viminfo=

command NoteSave :w <bar> !git commit -a --allow-empty-message -m ''
```

Add this command to your shell's config file (ex: `.bashrc`):

```
function vime() {
	local FILE="$1";
	if [ -f "$FILE" ];
	then
		vim -u ~/.vimencryptrc "$FILE"
	else
		vim -u ~/.vimencryptrc -x "$FILE"
	fi
}
```

